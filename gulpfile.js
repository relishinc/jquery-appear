var 
  gulp            = require('gulp'),
  gutil           = require('gulp-util'),
  concat          = require('gulp-concat'),
  header          = require('gulp-header'),
  minifyCss       = require('gulp-clean-css'),
  rename          = require('gulp-rename'),
  less            = require('gulp-less'),
  uglify          = require('gulp-uglify'),
  plumber         = require('gulp-plumber'),
  autoprefixer    = require('gulp-autoprefixer'),
  include         = require('gulp-include'),
	path            = require('path'),
	babel           = require('gulp-babel'),
	config          = require('./package.json')
  ;

var 
  dest    = './dist/',
  banner  = '/* Name: <%= name %> */\n/* Version: <%= version %> */\n/* Generated <%= (new Date()).toUTCString() %> */\n';



/*********************************
 * ERROR HANDLING / DEBUGGING *
 ********************************/
 
function handleErrors() {
  return plumber(function () {
    var args = [].slice.apply(arguments);

    // Send error to notification center with gulp-notify
    notify.onError({
      title: "Compile Error",
      message: "<" + "%= error.message %" + ">"
    }).apply(this, args);

    // Keep gulp from hanging on this task
    this.emit('end');
  });
}


/*********************************
 * CSS *
 ********************************/

gulp.task('css', function () {
	
  return gulp
  	.src([ 
  		'src/*.less' 
  	])
    .pipe(handleErrors())
    .pipe(less())
    .pipe(autoprefixer({
      browsers: ['last 2 versions']
    }))
    .pipe(concat(config.name + '.css'))
    .pipe(header(banner, { name: config.name, version: config.version }))
    .pipe(gulp.dest(dest))
    .pipe(minifyCss())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(header(banner, { name: config.name, version: config.version }))
    .pipe(gulp.dest(dest));
        
});

/*********************************
 * JS *
 ********************************/

gulp.task('js', function () {
	
  return gulp
  	.src([
	  	'src/*.js'
  	])
    .pipe(include().on('error', gutil.log))
    .pipe(concat(config.name + '.js'))
    .pipe(babel())
    .pipe(header(banner, { name: config.name, version: config.version }))
    .pipe(gulp.dest(dest))
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(header(banner, { name: config.name, version: config.version }))
    .pipe(gulp.dest(dest));
    
});

// Default task
gulp.task( 'default', gulp.series([ 'css', 'js' ], function(callback) {
	return callback();
}));

