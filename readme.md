# Appear

Animate stuff when the page loads.

### Installation

Install via Bower

```bash
bower install https://bitbucket.org/relishinc/jquery-appear.git --save
```

Include the JS and CSS files
```html
<script src="jquery.appear.min.js"></script>
<link rel="stylesheet" href="jquery.appear.min.css">
```


### Usage

Set up your elements

```html
<div data-appear="fade-in" data-appear-delay="500">
    I think I'll fade in...
</div>
```

Available attributes are:

`data-appear` *(required)* is the [name of a transition](#markdown-header-built-in-transitions)
`data-appear-duration` length of animation in ms (must be multiple of 50)
`data-appear-delay` delay to start animation in ms (must be multiple of 50)
`data-appear-easing` name of the [easing equation](#markdown-header-built-in-easing-equations)

Enable the animations in JS:

```javascript
$(document).ready(function() {
   APPEAR.init(options); 
});
```

### Options

Available options are:

`startEvent` an event that's triggered on the **document** object that tells the animations when to start (default: *'ready'*). See how to [trigger a custom event](#markdown-header-triggering-a-custom-startevent).
`easing` fallback easing equation for all animations (default: *'ease-in-out'*)
`duration` fallback duration in ms for all animations. Must be a multiple of 50 (default: *500*)
`delay` fallback delay in ms for all animations. Must be a multiple of 50 (default: *0*)

### Built-in transitions

* `fade`
* `fade-up`
* `fade-down`
* `fade-right`
* `fade-left`
* `fade-up-right`
* `fade-up-left`
* `fade-down-right`
* `fade-down-left`
* `slide-in-left`
* `slide-in-right`
* `slide-in-bottom`
* `slide-in-top`

### Adding custom transitions

Just add them to the CSS then you can refer to them in the HTML:

```scss
[data-appear="slide-in-left"] { // initial state
    transform: translate(-100%, 0);
    opacity: 0;
    transition-property: transform, opacity;
}
[data-appear="slide-in-left"].appear-animate { // final state
    transform: translate(0, 0);
    opacity: 1;
}
```

### Built-in easing equations

* `linear`
* `ease`
* `ease-in`
* `ease-out`
* `ease-in-out`
* `ease-in-back`
* `ease-out-back`
* `ease-in-out-back`
* `ease-in-sine`
* `ease-out-sine`
* `ease-in-out-sine`
* `ease-in-quad`
* `ease-out-quad`
* `ease-in-out-quad`
* `ease-in-cubic`
* `ease-out-cubic`
* `ease-in-out-cubic`
* `ease-in-quart`
* `ease-out-quart`
* `ease-in-out-quart`
* `ease-in-expo`
* `ease-out-expo`
* `ease-in-out-expo`
* `ease-in-circ`
* `ease-out-circ`
* `ease-in-out-circ`

### Adding custom easing equations

Just add them to the CSS then you can refer to them in the HTML:

```scss
body[data-appear-easing="ease-in-circ"] [data-appear],
[data-appear][data-appear][data-appear-easing="ease-in-circ"] {
    transition-timing-function: cubic-bezier(0.600, 0.040, 0.980, 0.335);
}
```

### Triggering a custom startEvent

You will need to add a [CustomEvent polyfill](https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent#Polyfill) to get this to work in Internet Explorer

```js
APPEAR.init({
    startEvent: 'myStartEvent'
});
document.dispatchEvent( new CustomEvent('myStartEvent') );
```

Must give credit to [AOS](https://github.com/michalsnik/aos) for providing some ideas for this library.
