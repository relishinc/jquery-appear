const APPEAR = ( () => 
{
  
  let _init = ( $options ) => 
  {
	  let defaults = {
		  startEvent: 'ready',
		  easing: 'ease-in-out',
		  duration: 500,
		  delay: 0
	  };
	  
    APPEAR.options = $.extend(true, {}, defaults, $options);
    
    // body tag attributes can override
    
    APPEAR.options.easing = $('body').data('appear-easing') || APPEAR.options.easing;
    APPEAR.options.duration = $('body').data('appear-duration') || APPEAR.options.duration;
    APPEAR.options.delay = $('body').data('appear-delay') || APPEAR.options.delay;
    
    $('body')
    	.attr('data-appear-easing', APPEAR.options.easing)
    	.attr('data-appear-duration', APPEAR.options.duration)
    	.attr('data-appear-delay', APPEAR.options.delay);
    
	  //<div 
	  //	data-appear="fade-in"                             // transition
	  // 	data-appear-easing="ease-in-out"                  // easing
	  //  data-appear-duration="500"                        // duration in ms
	  //  data-appear-delay="0"                             // delay in ms
	  //>
      
    $(document)
    	.on(APPEAR.options.startEvent, (e) => 
    	{
	    	$('[data-appear]:not(.appear-animate)')
	    		.addClass('appear-animate');
    	});
        
  };

  return {
    init: _init
  }

} )();